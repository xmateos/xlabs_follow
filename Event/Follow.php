<?php

namespace XLabs\FollowBundle\Event;

use Symfony\Component\EventDispatcher\Event;

class Follow extends Event
{
    const NAME = 'xlabs_follow.event';

    protected $follow;

    public function __construct($follow)
    {
        $this->follow = $follow;
    }

    public function getFollow()
    {
        return $this->follow;
    }
}