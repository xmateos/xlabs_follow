<?php

namespace XLabs\FollowBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use XLabs\FollowBundle\Event\Follow as FollowEvent;

class FollowController extends Controller
{
    /**
     * @Route("/example", name="xlabs_follow_example")
     */
    public function exampleAction()
    {
        return $this->render('XLabsFollowBundle:Follow:example.html.twig');
    }

    /**
     * @Route("/action", name="xlabs_follow_ajax", options={"expose"=true})
     */
    public function followAction()
    {
        $request = $this->get('request_stack')->getCurrentRequest();

        $data_xlabs_follow = stripslashes(file_get_contents("php://input"));
        $data_xlabs_follow = json_decode($data_xlabs_follow, true);
        $data_xlabs_follow = $data_xlabs_follow['data-xlabs-follow'];

        $user = $this->container->get('security.token_storage')->getToken()->getUser();
        $response = array(
            'currentStatus' => 0,
            'currentTotalFollowers' => 0,
        );
        $xlabs_follow_engine = $this->get('xlabs_follow_engine');
        if($request->isMethod('POST') && $data_xlabs_follow && !is_string($user))
        {
            $response['currentStatus'] = $xlabs_follow_engine->switchFollowStatus($data_xlabs_follow['entityType'], $data_xlabs_follow['entity_id']);

            // Dispatch event
            $event = new FollowEvent(array(
                'follower_id' => $user->getId(),
                'followedType' => $data_xlabs_follow['entityType'],
                'followed_id' => $data_xlabs_follow['entity_id'],
                'timestamp' => time(),
                'currentStatus' => $response['currentStatus']
            ));
            $this->get('event_dispatcher')->dispatch(FollowEvent::NAME, $event);
        }
        $response['currentTotalFollowers'] = $xlabs_follow_engine->getTotalFollowers($data_xlabs_follow['entityType'], $data_xlabs_follow['entity_id']);

        $response = new Response(json_encode($response, JSON_PRETTY_PRINT), 200);
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }
}
