<?php

namespace XLabs\FollowBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use \DateTime;

/**
 * @ORM\Entity
 * @ORM\Table(name="xlabs_follows", uniqueConstraints={@ORM\UniqueConstraint(name="unique_row",columns={"followed_id", "followed_type", "user_id"})}, indexes={@ORM\Index(name="search_idx", columns={"followed_id", "followed_type", "user_id"})})
 */
class Follow
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    public function getId()
    {
        return $this->id;
    }

    /**
     * @ORM\Column(name="followed_id", type="integer", nullable=false)
     */
    protected $followed_id;

    public function getFollowedId()
    {
        return $this->followed_id;
    }

    public function setFollowedId($followed_id)
    {
        $this->followed_id = $followed_id;
    }

    /**
     * @ORM\Column(name="followed_type", type="string", length=64, nullable=false)
     */
    protected $followed_type;

    public function getFollowedType()
    {
        return $this->followed_type;
    }

    public function setFollowedType($followed_type)
    {
        $this->followed_type = $followed_type;
    }

    /**
     * @ORM\Column(name="score", type="datetime", nullable=false)
     */
    private $score;

    public function getScore()
    {
        return $this->score;
    }

    public function setScore(DateTime $score)
    {
        $this->score = $score;
    }

    /*
     * @ORM\ManyToOne(targetEntity="XLabs\LikeBundle\Model\XLabsLikeUserInterface")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     **/
    /**
     * @ORM\Column(name="user_id", type="integer", nullable=false)
     */
    protected $user_id;

    public function getUserId()
    {
        return $this->user_id;
    }

    //public function setUser(XLabsLikeUserInterface $user)
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;
    }

    public function __construct($user_id, $followed_type, $followed_id, $score)
    {
        $this->user_id = $user_id;
        $this->followed_type = $followed_type;
        $this->followed_id = $followed_id;
        $this->score = $score;
    }
}