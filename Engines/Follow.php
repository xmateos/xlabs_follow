<?php

namespace XLabs\FollowBundle\Engines;

use Predis\Client as Predis;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use XLabs\FollowBundle\Queue\Producer as MysqlBackup;
use XLabs\FollowBundle\Entity\User;

class Follow
{
    private $config;
    private $redis;
    protected $user_in_session;
    private $mysql_backup;

    public function __construct($config, TokenStorageInterface $token_storage, MysqlBackup $mysql_backup)
    {
        $this->config = $config;
        $this->redis = new Predis(array(
            'scheme' => 'tcp',
            'host'   => $config['redis_settings']['host'],
            'port'   => $config['redis_settings']['port'],
            'database' => $config['redis_settings']['database_id']
        ), array(
            'prefix' => isset($config['_key_namespace']) ? $config['_key_namespace'].':' : ''
        ));
        $this->user_in_session = $token_storage->getToken() ? $token_storage->getToken()->getUser() : false;
        $this->mysql_backup = $mysql_backup;
    }

    public function disableLogging()
    {
        /*
         * For bulk operations, call this method to avoid PHP memory leaks
         */
        $this->redis->getConnection()->setLogger(null);
        return $this;
    }

    public function getInstance()
    {
        return $this->redis;
    }

    public function getAllKeys($redis_instance = false)
    {
        $redis_instance = $redis_instance ? $redis_instance : $this->redis;
        $keyPattern = "*";
        $keys = $redis_instance->keys($keyPattern);
        return $keys;
    }

    public function deleteAllKeys($redis_instance = false)
    {
        $redis_instance = $redis_instance ? $redis_instance : $this->redis;
        $keys = $this->getAllKeys();
        if($keys)
        {
            $prefix = $redis_instance->getOptions()->__get('prefix')->getPrefix();
            foreach($keys as $key)
            {
                $key = substr($key, strlen($prefix));
                $redis_instance->del(array($key));
            }
        }
    }

    public function changePrefix($old_prefix, $new_prefix)
    {
        $redis['generic'] = new Predis([
            'scheme' => 'tcp',
            'host'   => $this->config['redis_settings']['host'],
            'port'   => $this->config['redis_settings']['port'],
            'database' => $this->config['redis_settings']['database_id']
        ]);
        $keys = $this->getAllKeys($redis['generic']);
        if($keys)
        {
            foreach($keys as $key)
            {
                if(strlen(strstr($key, $old_prefix)) > 0)
                {
                    $redis['generic']->rename($key, str_replace($old_prefix, $new_prefix, $key));
                }
            }
        }
        $redis['generic']->disconnect();
    }

    public function setUser($user)
    {
        if(is_array($user))
        {
            $u = $user;
            $user = new User();
            $user->id = $u['id'];
        }
        $this->user_in_session = $user;
        return $this;
    }

    /**
     * Check user is following given $followingType having $following_id
     */
    public function follows($followingType, $following_id)
    {
        return is_null($this->redis->zScore("user:".$this->user_in_session->getId().":following:".$followingType, $following_id)) ? false : true;
    }

    public function follow($followedType, $followed_id, $score = false, $backup = true)
    {
        $score = $score ? $score : time();
        $this->redis->zAdd("user:".$this->user_in_session->getId().":following:".$followedType, array($followed_id => $score));
        $this->redis->zAdd($followedType.":".$followed_id.":followers:user", array($this->user_in_session->getId() => $score));
        $this->redis->zIncrBy("ranking:".$followedType, 1, $followed_id);
        if($backup)
        {
            $this->mysql_backup->process(array(
                'action' => 'follow',
                'user_id' => $this->user_in_session->getId(),
                'followed_type' => $followedType,
                'followed_id' => $followed_id,
                'score' => $score,
            ));
        }
        $currentStatus = 1;
        return $currentStatus;
    }

    public function unfollow($unfollowedType, $unfollowed_id, $backup = true)
    {
        $this->redis->zRem("user:".$this->user_in_session->getId().":following:".$unfollowedType, $unfollowed_id);
        $this->redis->zRem($unfollowedType.":".$unfollowed_id.":followers:user", $this->user_in_session->getId());
        $this->redis->zIncrBy("ranking:".$unfollowedType, -1, $unfollowed_id);
        if($backup)
        {
            $this->mysql_backup->process(array(
                'action' => 'unfollow',
                'user_id' => $this->user_in_session->getId(),
                'followed_type' => $unfollowedType,
                'followed_id' => $unfollowed_id,
                'score' => false,
            ));
        }
        $currentStatus = 0;
        return $currentStatus;
    }

    public function getFollowers($followedType, $followed_id, $showScores = false, $maxResults = false, $page = 1)
    {
        $offset = $maxResults ? (($page - 1)*$maxResults) : 0;
        $lastItem = $maxResults ? ($offset + ($maxResults - 1)) : -1;

        return $showScores ? $this->redis->zRange($followedType.":".$followed_id.":followers:user", $offset, $lastItem, 'withscores') : array_filter($this->redis->zRange($followedType.":".$followed_id.":followers:user", $offset, $lastItem), function($v){
            return $v != 'undefined';
        });
    }

    public function getFollowersByScore($followedType, $followed_id, $min_score, $max_score, $showScores = false, $maxResults = false, $page = 1)
    {
        $offset = $maxResults ? (($page - 1)*$maxResults) : 0;
        $count = $maxResults ? $maxResults : -1;

        return $showScores ? $this->redis->zRangeByScore($followedType.":".$followed_id.":followers:user", $min_score, $max_score, array('withscores' => true, 'limit' => array($offset, $count))) : array_filter($this->redis->zRangeByScore($followedType.":".$followed_id.":followers:user", $offset, $count), function($v){
            return $v != 'undefined';
        });
    }

    public function getFollowed($followedType, $showScores = false, $maxResults = false, $page = 1)
    {
        $offset = $maxResults ? (($page - 1)*$maxResults) : 0;
        $lastItem = $maxResults ? ($offset + ($maxResults - 1)) : -1;

        return $showScores ? $this->redis->zRange("user:".$this->user_in_session->getId().":following:".$followedType, $offset, $lastItem, 'withscores') : array_filter($this->redis->zRange("user:".$this->user_in_session->getId().":following:".$followedType, $offset, $lastItem), function($v){
            return $v != 'undefined';
        });
    }

    public function getTotalFollowers($followedType, $followed_id)
    {
        return $this->redis->zCard($followedType.":".$followed_id.":followers:user");
    }

    public function getTotalFollowed($followedType)
    {
        return $this->redis->zCard("user:".$this->user_in_session->getId().":following:".$followedType);
    }

    public function switchFollowStatus($followedType, $followed_id, $score = false)
    {
        return $this->follows($followedType, $followed_id) ? $this->unfollow($followedType, $followed_id) : $this->follow($followedType, $followed_id, $score);
    }

    public function getMostFollowed($followedType, $maxResults = false, $page = 1, $showScores = false)
    {
        $offset = $maxResults ? (($page - 1)*$maxResults) : 0;
        $lastItem = $maxResults ? ($offset + ($maxResults - 1)) : -1;
        if($showScores)
        {
            return array_filter($this->redis->zRevRange("ranking:".$followedType, $offset, $lastItem, 'withscores'), function($v, $k){
                return $v && $k;
            }, ARRAY_FILTER_USE_BOTH);
        } else {
            return array_filter($this->redis->zRevRange("ranking:".$followedType, $offset, $lastItem), function($v){
                return $v;
            });
        }
    }

    public function getTimeline($followedType, $maxResults = false, $page = 1)
    {
        $offset = $maxResults ? (($page - 1)*$maxResults) : 0;
        $lastItem = $maxResults ? ($offset + ($maxResults - 1)) : -1;
        return array_filter($this->redis->zRevRange("user:".$this->user_in_session->getId().":timelina:".$followedType, $offset, $lastItem), function($v){
            return $v;
        });
    }

    public function getTimelineByScore($followedType, $min_score, $max_score, $maxResults = false, $page = 1)
    {
        $offset = $maxResults ? (($page - 1)*$maxResults) : 0;
        $count = $maxResults ? $maxResults :  -1;
        return $this->redis->zRevRangeByScore("user:".$this->user_in_session->getId().":timelina:".$followedType, $max_score, $min_score, array('withscores' => false, 'limit' => array($offset, $count)));
    }

    public function getTotalItemsInTimeline($followedType)
    {
        return $this->redis->zCard("user:".$this->user_in_session->getId().":timelina:".$followedType);
    }

    public function getTotalItemsInTimelineByScore($followedType, $min_score, $max_score)
    {
        return $this->redis->zCount("user:".$this->user_in_session->getId().":timelina:".$followedType, $min_score, $max_score);
    }

    public function addToTimeline($followedType, $followed_id, $score = false)
    {
        $score = $score ? $score : time();
        $this->redis->zAdd("user:".$this->user_in_session->getId().":timelina:".$followedType, array($followed_id => $score));
        return true;
    }
}