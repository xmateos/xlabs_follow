<?php

namespace XLabs\FollowBundle\Extension;

use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use XLabs\FollowBundle\Engines\Follow as FollowEngine;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class FollowExtension extends AbstractExtension
{
    private $token_storage;
    private $xlabs_follow_engine;

    public function __construct(TokenStorageInterface $token_storage, FollowEngine $xlabs_follow_engine)
    {
        $this->token_storage = $token_storage;
        $this->xlabs_follow_engine  = $xlabs_follow_engine;
    }
    
    public function getFunctions()
    {
        return array(
            new TwigFunction('checkFollow', array($this, 'checkFollow')),
            new TwigFunction('getTotalFollowers', array($this, 'getTotalFollowers')),
            new TwigFunction('getTotalFollowed', array($this, 'getTotalFollowed')),
            new TwigFunction('getTimeline', array($this, 'getTimeline')),
        );
    }
    
    public function getFilters()
    {
        return array();
    }

    public function checkFollow($followedType, $followed_id)
    {
        $user = $this->token_storage->getToken()->getUser();
        return is_string($user) ? false : $this->xlabs_follow_engine->follows($followedType, $followed_id);
    }

    public function getTotalFollowers($followedType, $followed_id)
    {
        return $this->xlabs_follow_engine->getTotalFollowers($followedType, $followed_id);
    }

    public function getTotalFollowed($followedType, $user = false)
    {
        if($user)
        {
            $this->xlabs_follow_engine->setUser($user);
        }
        return $this->xlabs_follow_engine->getTotalFollowed($followedType);
    }

    public function getTimeline($followedType, $user = false, $maxResults = false, $page = 1)
    {
        if($user)
        {
            $this->xlabs_follow_engine->setUser($user);
        }
        return $this->xlabs_follow_engine->getTimeline($followedType, $maxResults, $page);
    }
}