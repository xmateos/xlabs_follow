<?php

namespace XLabs\FollowBundle\Queue;

use XLabs\RabbitMQBundle\RabbitMQ\Consumer as Parent_Consumer;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use XLabs\FollowBundle\Entity\Follow;
use \DateTime;
use \Exception;

class Consumer extends Parent_Consumer
{
    // set your custom consumer command name
    protected static $consumer = 'xlabs:follow:mysql_backup';

    // following function is required as it is
    protected function configure()
    {
        $this
            ->setName(self::$consumer)
        ;
    }

    // following function is required as it is
    public function getQueueName()
    {
        return Producer::getQueueName();
    }

    public function callback($msg)
    {
        // this is all sample code to output something on screen
        $container = $this->getApplication()->getKernel()->getContainer();

        $msg = json_decode($msg->body);

        $action = $msg->action;
        $user_id = $msg->user_id;
        $followed_type = $msg->followed_type;
        $followed_id = $msg->followed_id;
        $score = $msg->score;

        //$em = $container->get('doctrine.orm.default_entity_manager');
        $em = $this->getEntityManager();
        switch($action)
        {
            case 'follow':
                $now = new DateTime();
                $score = $now->setTimestamp($score);
                $follow = new Follow($user_id, $followed_type, $followed_id, $score);
                $em->persist($follow);
                break;
            case 'unfollow':
                $follow = $em->getRepository(Follow::class)->findOneBy(array(
                    'followed_id' => $followed_id,
                    'followed_type' => $followed_type,
                    'user_id' => $user_id
                ));
                if($follow)
                {
                    $em->remove($follow);
                }
                break;
        }
        try {
            $em->flush();
        } catch (Exception $e) {

        }
    }

    private function getEntityManager()
    {
        $container = $this->getApplication()->getKernel()->getContainer();
        $em = $container->get('doctrine.orm.default_entity_manager');
        if(!$em->isOpen())
        {
            $em = $em->create($em->getConnection(), $em->getConfiguration());
        }
        return $em;
    }
}