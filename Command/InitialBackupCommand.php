<?php

namespace XLabs\FollowBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\Console\Formatter\OutputFormatterStyle;
use XLabs\FollowBundle\Engines\Follow as FollowEngine;
use XLabs\FollowBundle\Entity\Follow;
use Symfony\Component\Console\Helper\ProgressBar;
use \DateTime;
use \Exception;

class InitialBackupCommand extends Command
{
    protected function configure()
    {
        $this
            ->setName('xlabs:follow:initial_backup')
            ->setDescription('Puts all redis data into MySQL')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getApplication()->getKernel()->getContainer();
        $em = $container->get('doctrine.orm.default_entity_manager');

        $output->writeln('');
        $output->writeln('<comment>::::::::::::::: BACK UP FOLLOWS :::::::::::::::</comment>');
        $output->writeln('<comment>::::::::::::::: Redis --> MySQL :::::::::::::::</comment>');
        $output->writeln('');

        $bundle_config = $container->getParameter('xlabs_follow_engine');
        if(array_key_exists('backup', $bundle_config) && is_array($bundle_config['backup']) && count($bundle_config['backup']))
        {
            $followable_entities = $bundle_config['backup'];
        } else {
            dump('No entities to be backed up found on the bundle configuration.');
            exit;
        }

        $follow_engine = $container->get(FollowEngine::class);

        $follows_counter = 0;
        foreach($followable_entities as $key => $followable_entity)
        {
            $qb = $em->createQueryBuilder();
            $entities = $qb
                ->select('e')
                ->from($followable_entity, 'e')
                ->getQuery();
            $progress = new ProgressBar($output, count($entities->getArrayResult()));
            $progress->start();
            $progress->setMessage('Redis Follows ['.$followable_entity.']');
            $progress->setFormat('[<info>%bar%</info>] %current%/%max% <comment>%message%</comment> %percent:3s%%');
            foreach($entities->iterate() as $e)
            {
                $e = $e[0];

                // Get followers
                $followers = $follow_engine->getFollowers($key, $e->getId(), $showScores = true, $maxResults = false);
                if(count($followers))
                {
                    foreach($followers as $follower_id => $score)
                    {
                        $now = new DateTime();
                        $score = $now->setTimestamp($score);
                        $follow = new Follow($follower_id, $key, $e->getId(), $score);
                        $em->persist($follow);
                        $follows_counter++;
                        /*try {
                            $em->flush();
                        } catch(Exception $exception) {

                        }*/
                    }
                }

                if($follows_counter % 100 == 0)
                {
                    $em->flush();
                    $em->clear();
                }

                $progress->advance();
            }
            $em->flush();

            $progress->finish();
        }
    }
}