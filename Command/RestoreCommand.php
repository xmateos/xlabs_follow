<?php

namespace XLabs\FollowBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\Console\Formatter\OutputFormatterStyle;
use XLabs\FollowBundle\Engines\Follow as FollowEngine;
use XLabs\FollowBundle\Entity\Follow;
use Symfony\Component\Console\Helper\ProgressBar;

class RestoreCommand extends Command
{
    protected function configure()
    {
        $this
            ->setName('xlabs:follow:restore')
            ->setDescription('Puts all MySQL data into redis. Optional user ids to restore only theirs.')
            ->addArgument('user_ids', InputArgument::IS_ARRAY, 'User ID')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getApplication()->getKernel()->getContainer();
        $em = $container->get('doctrine.orm.default_entity_manager');

        $follow_engine = $container->get(FollowEngine::class);

        $qb = $em->createQueryBuilder();
        $entities = $qb
            ->select('e')
            ->from(Follow::class, 'e');

        $user_ids = $input->getArgument('user_ids') ? $input->getArgument('user_ids') : false;
        if($user_ids)
        {
            $qb->where(
                $qb->expr()->in('e.user_id', $user_ids)
            );
        }
        $entities = $qb->getQuery();

        $output->writeln('');
        $output->writeln('<comment>:::::::::::::: RESTORING FOLLOWS ::::::::::::::</comment>');
        $output->writeln('<comment>::::::::::::::: MySQL --> Redis :::::::::::::::</comment>');
        $output->writeln('');

        $progress = new ProgressBar($output, count($entities->getArrayResult()));
        $progress->start();
        $progress->setMessage('Follows');
        $progress->setFormat('[<info>%bar%</info>] %current%/%max% <comment>%message%</comment> %percent:3s%%');
        foreach($entities->iterate() as $follow)
        {
            $follow = $follow[0];

            $follow_engine->setUser(array('id' => $follow->getUserId()))->follow($follow->getFollowedType(), $follow->getFollowedId(), $follow->getScore()->getTimestamp(), false);

            $progress->advance();
        }

        $progress->finish();

        $output->writeln('');
        $output->writeln('<comment>::::::::::::::::::::::: DONE ::::::::::::::::::::::</comment>');

        /*$follows = $em->getRepository(Follow::class)->findAll();
        if($follows)
        {
            foreach($follows as $follow)
            {
                $follow_engine->setUser(array('id' => $follow->getUserId()))->follow($follow->getFollowedType(), $follow->getFollowedId(), $follow->getScore()->getTimestamp());
            }
        }*/
    }
}