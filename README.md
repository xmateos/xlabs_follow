A redis driven like engine.

## Installation ##

Install through composer:

```bash
php -d memory_limit=-1 composer.phar require xlabs/followbundle
```

This bundle depends on "xlabs/rabbitmqbundle". Make sure to set it up too.

In your AppKernel

```php
public function registerbundles()
{
    return [
    	...
    	...
    	new XLabs\FollowBundle\XLabsFollowBundle(),
    ];
}
```

```bash
php bin/console doctrine:schema:update --force
```

## Routing

Append to main routing file:

``` yml
# app/config/routing.yml
  
xlabs_follow_engine:
    resource: "@XLabsFollowBundle/Resources/config/routing.yml"
    #prefix:   /
```

## Configuration sample
Default values are shown below:
``` yml
# app/config/config.yml
  
x_labs_follow:
    redis_settings:
        host: 192.168.5.23
        port: 6379
        database_id: 7
    _key_namespace: 'xlabs:follow'
    backup: # for mysql backup/restore
        <alias>: <entity_FQCN>
```

### Usage ###
Append this anywhere in you template
```php
{% include 'XLabsFollowBundle:Follow:loader.html.twig' %}
```

To see a sample template, check:
```php
XLabsFollowBundle:Follow:example.html.twig
```

### MySQL Backup ###
Make sure you run the following command. This is the consumer that will save all operations in the project DB.
``` bash
php bin/console xlabs:follow:mysql_backup --no-debug
```
If ever Redis lost all the data, you will be able to recover it by issueing the following command:
``` bash
php bin/console xlabs:follow:restore --no-debug
```
If you already have data in redis and want to create a mysql backup of it, there´s a sample one-time command you should copy to your project and adapt conveniently:
``` bash
php bin/console xlabs:follow:initial_backup --no-debug
```

### Event listener ###
If you want some action to take place whenever a FOLLOW takes place in the frontends, you can create an event listener as follows:
``` yml
# YourBundle/Resources/config/services.yml
    ...
    xlabs_follow.event_listener:
        class:  YourBundle\EventListeners\YourListener.php
        tags:
            - { name: kernel.event_listener, event: xlabs_follow.event, method: yourListenerMethod }
```
```php
use Symfony\Component\EventDispatcher\Event;
  
class YourListener extends Event
{
    public function yourListenerMethod(Event $event)
    {
        dump($event->getFollow()); die;
    }
}
```
The $event variable contains all info about the FOLLOW action that has taken place.

### Tweaking ###
By default, the service uses the user in session. If you ever wanted to use the service on your own by using any specific user performing the FOLLOW action:
```php
$user = $em->getRepository('YourBundle:YourUserEntity')->find(<ID>);
$follow_engine = $container->get('xlabs_follow_engine');
$follow_engine->setUser($user);
$follow_engine->...
```